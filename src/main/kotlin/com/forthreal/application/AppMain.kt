/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application

import com.forthreal.application.classes.orient.OrientConnection
import com.forthreal.application.classes.orient.OrientOperations
import com.forthreal.application.webapi.WebInstance
import com.forthreal.application.functions.*

object AppMain
{
    @JvmStatic
    fun main(args: Array<String>)
    {

        val orientDBconnection =
              OrientConnection("192.168.10.10","social", "dbuser", "dbuser")

        val orientOperations = OrientOperations( orientDBconnection )

        val webInstance = WebInstance(8040, orientDBconnection, orientOperations )

        webInstance.setURIHanlder("/users/new", "post", ::userNewFun )
        webInstance.setURIHanlder("/communities/new", "post", ::communityNewFun )

    }
}
