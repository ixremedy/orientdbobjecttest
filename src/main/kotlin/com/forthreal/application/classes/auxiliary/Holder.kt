/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.classes.auxiliary

class Holder<T>
{
    var value: T?

    constructor()
    {
        value = null
    }

    constructor(variable: T)
    {
        value = variable
    }
}