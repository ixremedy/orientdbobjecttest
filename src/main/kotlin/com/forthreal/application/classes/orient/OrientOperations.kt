/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.classes.orient

import com.forthreal.application.classes.ObjectPatternParser
import com.orientechnologies.orient.core.exception.OCommandExecutionException
import com.orientechnologies.orient.core.metadata.schema.OClass
import com.orientechnologies.orient.core.metadata.schema.OType
import com.orientechnologies.orient.core.record.ORecord

class OrientOperations(val connection: OrientConnection)
{
    fun checkObjectExists(vertexName: String, propertyName: String, propertyValue: String) : Boolean
    {
        val session = connection.getDBSession()

        val count =
            session
                .query(
                    "SELECT COUNT(*) AS count FROM ${vertexName} WHERE ${propertyName} = ?",
                    propertyValue
                )
                .next()
                .getProperty<Long>("count")

        session.close()

        return count > 0
    }

    /* we will do only simple signalling the result through a boolean,
    * in a real project, it would be more sophisticated */
    fun conductLinking(
        sourceObject: String,
        destinationObject: String
    ) : Boolean
    {
        val sourceResult = ObjectPatternParser.parseString( sourceObject )
        val destinationObject = ObjectPatternParser.parseString( destinationObject )

        val session = connection.getDBSession()

        /* if the format of both objects is Okay */
        if( ( sourceResult.isPresent == true ) && ( destinationObject.isPresent == true ) )
        {
            val result =
                session
                    .query(
                        "CREATE EDGE FROM " +
                                " (SELECT FROM ${sourceResult.get().first} WHERE ${sourceResult.get().second} = ? ) " +
                                " TO " +
                                " (SELECT FROM ${destinationObject.get().first} WHERE ${destinationObject.get().second} = ? ) ",
                        sourceResult.get().third,
                        destinationObject.get().third
                      )

            println("result " + result)

            return true
        }

        session.close()

        return false
    }

    fun addNewCommunity(name: String)
    {
        val session = connection.getDBSession()

        session.use {
            /* add the vertex */
            val vertex = session.newVertex("Community")
            vertex.setProperty("name", name)
            vertex.save<ORecord>()
        }
    }

    fun addNewSocialUser(username: String, email: String)
    {
        val session = connection.getDBSession()

        session.use {
            /* add the vertex */
            val vertex = session.newVertex("SocialUser")
            vertex.setProperty("name", username )
            vertex.setProperty("email", email )
            vertex.save<ORecord>()
        }
    }

    fun checkClassExists(className: String) : Boolean
    {
        val session = connection.getDBSession()

        session.use {
            val userClass = session.metadata.schema.existsClass(className)

            println("Class exists: ${className} ${userClass}")

            return userClass
        }

    }

    /* create Vertex type Community */
    fun createCommunityType()
    {
        val session = connection.getDBSession()

        session.use {
            try
            {
                val vertClass = session.createVertexClass("Community")
                vertClass.createProperty("name", OType.STRING)
                vertClass.createIndex("name", OClass.INDEX_TYPE.UNIQUE)
            }
            /* catch class already exists exception */
            catch (exc: OCommandExecutionException) { }
        }
    }

    /* create Vertex type SocialUser */
    fun createSocialUserType()
    {
        val session = connection.getDBSession()

        session.use {
            try
            {
                val vertClass = session.createVertexClass("SocialUser")
                vertClass.createProperty("name", OType.STRING)
                vertClass.createProperty("email", OType.STRING)
                vertClass.createIndex("email", OClass.INDEX_TYPE.UNIQUE)
            }
            /* catch class already exists exception */
            catch (exc: OCommandExecutionException) { }
        }
    }
}