/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.webapi

import com.forthreal.application.classes.orient.OrientConnection
import com.forthreal.application.classes.orient.OrientOperations
import io.javalin.Javalin
import io.javalin.http.Context

public class WebInstance
{
    val application: Javalin
    val orientSession: OrientConnection
    val orientOperations: OrientOperations

    constructor(port: Int, orientSession: OrientConnection, orientOperations: OrientOperations)
    {
        this.orientSession = orientSession
        this.orientOperations = orientOperations

        application = Javalin.create().start( port )

        /* dependency injection - handler of orientDB */
        application.before { context -> context.register( OrientConnection::class.java, orientSession )  }
        application.before { context -> context.register( OrientOperations::class.java, orientOperations ) }
    }

    public fun setURIHanlder(uri: String, method: String, function: (Context) -> String?)
    {

        when ( method.toLowerCase() )
        {
            "get" ->
                application.get( uri ) {
                        context ->

                            val result = function( context )

                            result?.let {
                                context.result( it )
                            }
                    }
            "post" ->
                application.post( uri ) {
                        context ->

                            val result = function( context )

                            result?.let {
                                context.result( it )
                            }
                }
        }

    }
}