/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.classes.validation

import com.alibaba.fastjson.JSONObject

class ValidateJSON(val jsonObject: JSONObject)
{
    companion object
    {
        val emailPattern = "[a-z0-9A-Z._-]{1,}@[a-z0-9A-Z-.]{1,}\\.[a-zA-Z]{1,6}"
    }

    enum class Type
    {
        EXISTS,
        DOESNT_EXIST,
        NOT_BLANK,
        VALID_EMAIL
    }

    fun checkProperties(vararg args: Pair<String, Type>) : Pair<Boolean, String>
    {
        for( arg in args )
        {
            when( arg.second )
            {
                Type.EXISTS ->
                    if( jsonObject.containsKey( arg.first) == false )
                    {
                        return Pair( false, arg.first )
                    }

                Type.DOESNT_EXIST ->
                    if( jsonObject.containsKey( arg.first ) == true )
                    {
                        return Pair( false, arg.first )
                    }

                Type.NOT_BLANK ->
                    if(
                        (jsonObject.containsKey( arg.first ) == false )
                        || ( jsonObject.getString( arg.first ).trim().length == 0 )
                       )
                    {
                        return Pair( false, arg.first )
                    }

                Type.VALID_EMAIL ->
                    if(
                        (jsonObject.containsKey( arg.first ) == false )
                        || ( jsonObject.getString( arg.first ).trim().length == 0 )
                        || ( jsonObject.getString( arg.first ).trim().matches( Regex(emailPattern) ) == false )
                      )
                    {
                        return Pair( false, arg.first )
                    }

            }
        }

        return Pair( true, "" )
    }
}