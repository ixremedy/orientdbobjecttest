/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.classes.orient

import com.forthreal.application.classes.auxiliary.Holder
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.db.OrientDB
import com.orientechnologies.orient.core.db.OrientDBConfig

class OrientConnection
{
    val orient: OrientDB
    val hLastSession: Holder<ODatabaseSession>

    val dbName: String
    val username: String
    val password: String

    constructor(host: String, dbName: String, username: String, password: String)
    {
        orient = OrientDB("remote:${host}", OrientDBConfig.defaultConfig() )
        hLastSession = Holder<ODatabaseSession>()

        this.dbName = dbName
        this.username = username
        this.password = password
    }

    public fun getDBSession() : ODatabaseSession
    {
        val session = orient.open( dbName, username, password )

        hLastSession.value = session

        return session
    }

    public fun getLastDBSession() : ODatabaseSession?
    {
        return hLastSession.value
    }
}