/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.classes

import java.util.*
import java.util.regex.Pattern

object ObjectPatternParser
{
    val sourceFormat = "([a-zA-Z]{1}[a-zA-Z0-9]{0,})\\[([a-zA-Z]{1}[a-zA-Z0-9]{0,})=([a-zA-Z0-9]{1,})\\]"

    fun parseString(input: String) : Optional< Triple<String, String, String> >
    {
        val pattern = Pattern.compile( sourceFormat )
        val matched = pattern.matcher( input )

        if( matched.matches() == false )
        {
            return Optional.empty()
        }

        val objectName = matched.group( 1 )
        val parameterName = matched.group( 2 )
        val value = matched.group( 3 )

        return Optional.of( Triple( objectName, parameterName, value) )
    }
}