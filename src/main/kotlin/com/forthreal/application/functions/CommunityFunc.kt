/*
** Developed by Lev Vanyan, lev.vanyan@forthreal.com
**/

package com.forthreal.application.functions

import com.alibaba.fastjson.JSONObject
import com.forthreal.application.classes.orient.OrientConnection
import com.forthreal.application.classes.orient.OrientOperations
import com.forthreal.application.classes.validation.ValidateJSON
import com.orientechnologies.orient.core.exception.OCommandExecutionException
import com.orientechnologies.orient.core.metadata.schema.OClass
import com.orientechnologies.orient.core.metadata.schema.OType
import com.orientechnologies.orient.core.record.ORecord
import io.javalin.http.Context

fun communityNewFun(context: Context) : String
{
    val jsonResponse = JSONObject()
    val body = context.body()

    context.contentType("application/json" )

    try
    {
        val jsonObject = JSONObject.parseObject( body )
        val validate = ValidateJSON( jsonObject )

        val isValid =
            validate.checkProperties (
                Pair("name", ValidateJSON.Type.NOT_BLANK),
                Pair("@SocialUser:email", ValidateJSON.Type.VALID_EMAIL)
            )

        if( isValid.first == true )
        {
            val orientOp = context.use( OrientOperations::class.java )

            val email = jsonObject.getString( "@SocialUser:email" )
            val found = orientOp.checkObjectExists("SocialUser", "email", email)

            /* object not found */
            if( found == false )
            {
                jsonResponse.putAll(
                    mapOf(
                        Pair("status", 1),
                        Pair("message", "Error: SocialUser with email ${email} not found")
                    )
                )

                return jsonResponse.toJSONString()
            }


            val communityName = jsonObject.getString("name")

            if( orientOp.checkClassExists("Community" ) == false )
            {
                orientOp.createCommunityType()
            }

            val communityExists = orientOp.checkObjectExists("Community", "name", communityName )

            /* there is no user with this email yet */
            if( communityExists == false )
            {

                orientOp.addNewCommunity( communityName )
                orientOp.conductLinking(
                    "Community[name=${communityName}]",
                    "SocialUser[email=${email}]"
                          )

                jsonResponse.putAll(
                      mapOf(
                          Pair("status", 0),
                          Pair("message", "Community successfully created")
                            )
                       )
            }
        }
        else
        {
            jsonResponse.putAll(
                mapOf(
                    Pair("status", 2),
                    Pair("message", "Error: field ${isValid.second} is missing or invalid")
                )
            )
        }
    }
    catch(exc: Exception)
    {
        exc.printStackTrace()
    }

    return jsonResponse.toJSONString()
}